// array that holds numbers in string form
let numArr = new Array();
numArr[0] = "";
numArr[1] = "One";
numArr[2] = "Two";
numArr[3] = "Three";
numArr[4] = "Four";
numArr[5] = "Five";
numArr[6] = "Six";
numArr[7] = "Seven";
numArr[8] = "Eight";
numArr[9] = "Nine";
numArr[10] = "Ten";
numArr[11] = "Eleven";
numArr[12] = "Twelve";
numArr[13] = "Thirteen";
numArr[14] = "Fourteen";
numArr[15] = "Fifteen";
numArr[16] = "Sixteen";
numArr[17] = "Seventeen";
numArr[18] = "Eighteen";
numArr[19] = "Nineteen";
numArr[20] = "Twenty";
numArr[30] = "Thirty";
numArr[40] = "Forty";
numArr[50] = "Fifty";
numArr[60] = "Sixty";
numArr[70] = "Seventy";
numArr[80] = "Eighty";
numArr[90] = "Ninety";

// function that converts number passed into a string of exact number
function numToStringConvert(num) {
  //variable to hold output of number converted to string
  let numToString = "";

  //variables to get place value of each digit in number
  let single = num % 10;
  let tens = parseInt(Math.floor((num / 10) % 10) + "0");
  let hundred = Math.floor((num / 100) % 10);

  let singlesTo20 = num % 100;

  //if else to output number into numToString in string format
  if (num <= 20) {
    numToString = numArr[num];
  } else if (num >= 20 && num < 100) {
    numToString = numArr[tens] + " " + numArr[single];
  } else if (num >= 100 && num < 1000) {
    if (num % 100 <= 20) {
      numToString = numArr[hundred] + " hundred " + numArr[singlesTo20];
    } else {
      numToString =
        numArr[hundred] + " hundred " + numArr[tens] + " " + numArr[single];
    }
  } else if (num === 1000) {
    numToString = "One Thousand";
  }
  console.log(numToString);

  return numToString;
}

//write numbers 1 to 1000 in html
for (let i = 0; i <= 1000; i++) {
  document.write("<br>");
  document.write(numToStringConvert(i));
  //numToStringConvert(i);
}
